import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RegisterModalComponent } from 'src/app/components/register-modal/register-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private dialogRef: MatDialog) {}

  ngOnInit(): void {
    console.log('home');
  }

  openDialog() {
    this.dialogRef.open(RegisterModalComponent, {
      width: '754px',
      height: '702px',
      panelClass: 'register-dialog',
    });
  }
}
