import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.scss'],
})
export class RegisterModalComponent implements OnInit {
  form = this.fb.group(
    {
      document: ['', Validators.required],
      company: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      user: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    },
    { validator: this.passwordMatchValidator }
  );

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    console.log('Register Modal');
  }

  passwordMatchValidator(frm: FormGroup) {
    return frm.controls['password'].value ===
      frm.controls['confirmPassword'].value
      ? null
      : { mismatch: true };
  }

  save() {
    console.log('save', this.form);
    const { valid, value } = this.form;

    if (valid) {
      // Lógica para salvar no backend
      console.log('Form: ', value);
    }
  }
}
