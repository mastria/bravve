module.exports = {
  'src/**/*.ts': (filesPath) => {
    return [
      `node_modules/.bin/eslint -c .eslintrc.json ${filesPath.join(' ')}`,
    ];
  },
  'projects/design/**/*.ts': (filesPath) => {
    return [
      `node_modules/.bin/eslint -c projects/design/.eslintrc.json  ${filesPath.join(
        ' '
      )}`,
    ];
  },
  '*{.ts,.html,.scss,.json,.js}': ['npm run style:fix'],
};
