# STAGE 1 - BUILDING APP

#FROM node:16.14 as node
FROM nginx:1-alpine

ARG APP_RELEASE="---"
ARG APP_COMMIT="---"

ENV PROJECT_NAME=${PROJECT_NAME} \
    APP_RELEASE=${APP_RELEASE} \
    APP_COMMIT=${APP_COMMIT}

#WORKDIR /app

#RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
#RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
#RUN apt-get update && apt-get install -y google-chrome-stable

#COPY . .

#RUN npm install
#RUN npm install -g @angular/cli
#RUN npm run lint
#RUN npm run build --prod

#ENV CHROME_BIN=/usr/bin/google-chrome
#ENTRYPOINT npm run test:ci

# STAGE 2 - DEPLOYING APP

#FROM nginx:1-alpine

#COPY --from=node /app/dist /usr/share/nginx/html
COPY ${PWD}/dist  /usr/share/nginx/html

EXPOSE 80

HEALTHCHECK --interval=10s --timeout=5s --retries=5 CMD curl --fail http://localhost:80/||exit 1
